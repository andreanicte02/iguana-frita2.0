import {NodoAst} from "./nodes/NodoAst.js";
import {ZAmbito} from "./types/ZAmbito.js";
import {Codigo} from "./nodes/Codigo.js";
import {FileLocation} from "./nodes/FileLocation.js";
import {
    codigoGenerarBloque,
    generarEtiqueta,
    getTextoDeclaracionTemporales,
    reiniciarContadores,
    tablaFunciones
} from "./Utilidades.js";
import { NTipoVar } from "./types/NTipo.js";
import {nTipoVoid} from "./types/TiposPrimitivos.js";
import {codigoInvocar} from "./types/codigos.js";
import { tablaSimbolos } from './Utilidades'
import {NombreVars} from "./NombreVars";
import {NombreFunciones} from "./NombreFunciones";

export { NodoObtenerTipoInteger, NodoObtenerTipoDouble } from './nodes/NodoObtenerTipoInteger.js'
export { NodoObtenerTipoBool } from './nodes/NodoObtenerTipoBool';
export { NodoCrearInteger } from './nodes/NodoCrearInteger';
export { NodoCrearTrue } from './nodes/NodoCrearTrue';
export { NodoCrearFalse } from './nodes/NodoCrearFalse';
export { NodoDeclararParametro } from './nodes/NodoDeclararParametro';
export { FileLocation } from './nodes/FileLocation' ;
export { NodoDeclararVar } from  './nodes/NodoDeclararVar';
export { NodoDeclararVarTipo5 } from  './nodes/NodoDeclararVarTipo5';
export { NodoDeclararVarTipo1 } from './nodes/NodoDeclararVarTipo1';
export { NodoSuma } from './nodes/arithmetic_operations/NodoSuma';
export { NodoResta } from './nodes/arithmetic_operations/NodoResta';
export { NodoMulti } from './nodes/arithmetic_operations/NodoMulti';
export { NodoModular } from  './nodes/arithmetic_operations/NodoModular';
export { NodoUnarioMenos } from  './nodes/arithmetic_operations/NodoUnarioMenos';
export { NodoInvocar, NodoInvocacionPunto } from './nodes/NodoInvocar';
export { NodoRelacional } from './nodes/NodoRelacional';
export { NodoObtenerTipoVoid } from './nodes/NodoObtenerTipoVoid';
export { NodoAsignar } from './nodes/NodoAsignar';
export { NodoObtenerVar } from './nodes/NodoObtenerVar';
export { NodoDeclararFuncion } from './nodes/NodoDeclararFuncion';
export { NodoInstanciar } from './nodes/NodoInstanciar';
export { NodoObtenerTipo } from './nodes/NodoObtenerTipo';
export { NodoAsignarAtributo } from './nodes/NodoAsignarAtributo';
export { NodoDeclararAtributo } from './nodes/NodoDeclararAtributo';
export { NodoDeclararEstructura } from './nodes/NodoDeclararEstructura';
export { NodoNot } from './nodes/NodoNot';
export { NodoOr } from './nodes/NodoOr';
export { NodoAnd } from './nodes/NodoAnd';
export { NodoXor } from './nodes/NodoXor';
export { NodoAccesoPunto } from './nodes/NodoAccesoPunto';
export { NodoPrintSimple } from './nodes/NodoPrintSimple';
export { NodoIf } from './nodes/control_sentences/NodoIf';
export { NodoWhile } from  './nodes/control_sentences/NodoWhile';
export { NodoDoWhile } from  './nodes/control_sentences/NodoDoWhile';
export { NodoFor } from './nodes/control_sentences/NodoFor';
export { NodoObtenerTipoChar } from './nodes/NodoObtenerTipoChar';
export { NodoCrearChar } from './nodes/NodoCrearChar';
export { NodoSwitch } from './nodes/control_sentences/NodoSwitch';
export { NodoCase } from './nodes/control_sentences/NodoCase';
export { NodoBreak } from  './nodes/control_sentences/NodoBreak';
export { NodoContinue } from './nodes/control_sentences/NodoContinue';
export { NodoCrearArrayConValores } from './nodes/NodoCrearArrayConValores';
export { NodoIgualdadReferencia } from './nodes/equal_operations/NodoIgualdadReferencia';
export { NodoIgualdadValor } from './nodes/equal_operations/NodoIgualdadValor';
export { NodoDesigualdadValor } from './nodes/equal_operations/NodoDesigualdadValor';
export { NodoCrearString } from './nodes/NodoCrearString';
export { NodoCrearArray } from './nodes/NodoCrearArray';
export { NodoAccessoArray } from './nodes/NodoAccessoArray';
export { NodoCrearDouble } from './nodes/NodoCrearDouble';
export { NodoCrearNull } from './nodes/NodoCrearNull';
export { NodoReturn } from './nodes/control_sentences/NodoReturn';
export { NodoDivision } from './nodes/arithmetic_operations/NodoDivision';
export { NodoCharCast } from './nodes/casteos_explicitos/NodoCharCast';
export { NodoIntegerCast } from './nodes/casteos_explicitos/NodoIntegerCast';
export { NodoDoubleCast } from './nodes/casteos_explicitos/NodoDoubleCast';
export { NodiInvocarSayajin } from './nodes/NodoInvocar';
export { NodoPotencia } from './nodes/arithmetic_operations/NodoPotencia';
export { NodoAumento } from  './nodes/arithmetic_operations/NodoAumento';
export { NodoDeclararVarTipo2 } from  './nodes/NodoDeclararVarTipo2';
export { NodoDeclararVarTipo4 } from  './nodes/NodoDeclararVarTipo4';
export { NodoDeclararVarTipo3 } from  './nodes/NodoDeclararVarTipo3';
export function generar3d(sentencias: NodoAst[]): string {

    console.log('sentencias:');
    console.log(sentencias);

    reiniciarContadores();

    const codigo = new Codigo();
    codigo.agregar(`var H, P;`);
    codigo.agregar(`P = 1;`); // comenzamos desde 1, por que el 0 se toma como null a la hora de manejar referencias
    codigo.agregar(`H = 1;`); // comenzamos desde 1, por que el 0 se toma como null a la hora de manejar referencias
    codigo.agregar('//');
    const ambito = new ZAmbito(undefined);
    const codigoAmbitoGlobal = codigoGenerarBloque(sentencias, ambito);
    const codigoFunciones = ambito.generarCodigoFunciones();
    console.log(ambito);
    const etiqutaInicio = generarEtiqueta();
    codigo.agregarCodigo(codigoAmbitoGlobal);

    codigo.agregar(`goto ${etiqutaInicio};`);
    codigo.comentar('etiqueta inicio');
    codigo.agregarCodigo(codigoFunciones);

    
    codigo.agregar(`${etiqutaInicio}:`);
    codigo.comentar('etiqueta inicio llamda a main');

    // codigo.agregar(`H = ${ambito.espacioEnMemoria};`)

    codigo.agregar('//////////////////////////////');
    codigo.agregar('// ejecucion de main');
    const codigoInvocarMain = codigoInvocar(ambito, 'principal', []);
    codigo.agregarCodigo(codigoInvocarMain);

    codigo.declararTemporales(`${getTextoDeclaracionTemporales()}`);
    return codigo.texto;
}

// Nos va a ayudar a crear el FileLocation de manera mas simplificada
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function fl(yy: any, pos: any): FileLocation {
    const filePath = yy.last();
    const x = pos.first_column;
    const y = pos.first_line;
    return new FileLocation(filePath, x, y);
}

export function getTS(): NombreVars[] {
    return tablaSimbolos;
}

export function getFunctions(): NombreFunciones []{
    return tablaFunciones;
}
